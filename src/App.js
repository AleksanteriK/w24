import logo from './logo.svg';
import './App.css';
import Month from './Month';
import Day from './Day';

function App() {
  return (
    <div className="App">
      <Month>
        <Day/>
      </Month>
    </div>
  );
}

export default App;
