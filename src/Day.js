import React from "react";

const Day = ({ month, day }) => {
   const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
   const startDayOfWeek = weekdays[new Date(2021, month - 1, day).getDay()];

   return (
      <div>
         {`Month ${month}/2021 starts ${startDayOfWeek}`}
      </div>
   );
};

export default Day;
