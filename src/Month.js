import React, { useState } from 'react';
import Day from './Day';

const Month = () => {
   const [selectedNumber, setSelectedNumber] = useState(1);
   const monthNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

   const handleMonthChange = (event) => {
      setSelectedNumber(parseInt(event.target.value, 10));
   };

  const selectionOfMonths = monthNumbers.map((number, index) => (
    <option key={index} value={number}>
      {number}
    </option>
  ));

   return (
      <div>
         <select onChange={handleMonthChange} value={selectedNumber}>
            {selectionOfMonths}
         </select>
      <Day month={selectedNumber} day={1} />
      </div>
   );
};

export default Month;